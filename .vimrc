" Crucial
set nocompatible

" Basic settings.
syntax enable
set t_Co=256
color jellybeans
set number
set backspace=indent,eol,start
set autoindent
set copyindent
set showmatch
set smartcase
set hlsearch
set incsearch
set history=500
set undolevels=500

" Handle indentation. 4 spaces behave like a tab, but aren't.
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

" Macbook Pro specific- the esc 'button' is terrible.
inoremap jj <Esc>

" Move cursor by display lines, not real lines.
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk
vnoremap <Down> gj
vnoremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk
noremap <silent> <leader>obk :call OpenCurrentFileBackupHistory()<cr>
map <silent> <C-E> :Vex<CR>

" Easy window/pane navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-Down> <C-w>j
map <C-Up> <C-w>k
map <C-Left> <C-w>h
map <C-Right> <C-w>l

" NERDtree like netrw
let g:netrw_banner = 0
let g:netrw_dirhistmax = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_winsize = 25
augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * if !argc() | :Vexplore  | endif
augroup END

" w!! will try to sudo write.
cmap w!! w !sudo tee % >/dev/null

" Auto shebang and encoding embeds depending on filetype.
augroup Shebang
  autocmd BufNewFile *.py 0put =\"#!/usr/bin/env python3\<nl># -*- coding: utf-8 -*-\<nl>\"|$
  autocmd BufNewFile *.rb 0put =\"#!/usr/bin/env ruby\<nl># -*- coding: None -*-\<nl>\"|$
  autocmd BufNewFile *.tex 0put =\"%&plain\<nl>\"|$
  autocmd BufNewFile *.\(cc\|hh\) 0put =\"//\<nl>// \".expand(\"<afile>:t\").\" -- \<nl>//\<nl>\"|2|start!
  autocmd FileType c,cpp,java,php,python autocmd BufWritePre <buffer> %s/\s\+$//e
augroup END

" vim_git_backups
augroup custom_backup
  autocmd!
  autocmd BufWritePost * call BackupCurrentFile()
augroup end

let s:custom_backup_dir=get(g:, 'vim_git_backups_directory', '~/.vim_git_backups')

function! BackupCurrentFile()
  if executable('git')
    if !isdirectory(expand(s:custom_backup_dir))
      let cmd = 'mkdir -p ' . s:custom_backup_dir . ';'
      let cmd .= 'cd ' . s:custom_backup_dir . ';'
      let cmd .= 'git init;'
      call system(cmd)
    endif
    let file = expand('%:p')
    if file =~ fnamemodify(s:custom_backup_dir, ':t') | return | endif
    let file_dir = s:custom_backup_dir . expand('%:p:h')
    let backup_file = s:custom_backup_dir . file
    let cmd = 'sh -c "'
    if !isdirectory(expand(file_dir))
      let cmd .= 'mkdir -p ' . file_dir . ';'
    endif
    let cmd .= 'cp ' . file . ' ' . backup_file . ';'
    let cmd .= 'cd ' . s:custom_backup_dir . ';'
    let cmd .= 'git add ' . backup_file . ';'
    let cmd .= 'git commit -m "Backup - `date`";'
    let cmd .= '"'
    call system(cmd)
  endif
endfunction

function! OpenCurrentFileBackupHistory()
  let backup_dir = expand(s:custom_backup_dir . expand('%:p:h'))
  let cmd = "cd " . backup_dir
  let cmd .= "; git log -p --since='1 month' " . expand('%:t')

  silent! exe "noautocmd botright pedit vim_git_backups"
  noautocmd wincmd P
  set buftype=nofile
  exe "noautocmd r! ".cmd
  exe "normal! gg"
  noautocmd wincmd p
endfunction

autocmd FileType python set cc=110
autocmd FileType c,cpp,java,php,sh set cc=80
autocmd FileType html set ft=htmljinja

" Easy addon management.
execute pathogen#infect()

" Local Machine Settings
source ${HOME}/.vimrc.local
