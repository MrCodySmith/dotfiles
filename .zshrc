#!/bin/zsh
################################################################################
#                   MrCodySmith's zshrc file, v1.0, based on:
#                           spease's zshrc file v2.0.1 
#
#
################################################################################

################################################################################
# Set general ZSH options.
#
#

# History
setopt SHARE_HISTORY      # Instant export of commands to history file+history
setopt APPEND_HISTORY     # Allow multiple zsh sessions to coexist.
setopt EXTENDED_HISTORY   # Puts timestamps in the history.
setopt HIST_IGNORE_DUPS   # Do not allow contiguous duplicates in history.

# Completion
setopt CORRECT            # Try to correct bad commands. e.g. cta -> cat
setopt CORRECT_ALL        # Try to correct bad arguments. e.g. --hepl -> --help
setopt REC_EXACT          # Allow exact matches without beeping at me.
setopt AUTO_LIST          # Automatically list choices for ambiguous completes.

# Directories
setopt PUSHD_TO_HOME      # pushd with no args assumes home directory.
setopt PUSHD_SILENT       # pushd does not tell me things I already know.
setopt AUTO_PUSHD         # cd is considered a call to pushd.
setopt AUTO_PARAM_SLASH   # Do not have an extra character to type.
setopt EXTENDED_GLOB      # nice features like recursive globbing, negation.

# Jobs
setopt NOTIFY             # Report BG jobs immediately, not at next prompt.
setopt LONG_LIST_JOBS     # List jobs in long format by default.
setopt AUTO_RESUME        # Allow a repitition of a bg command to resume it.

# Do not allow: I hate these settings.
unsetopt BG_NICE          # Background commands run at same priority as FG
unsetopt MENUCOMPLETE     # When there is completions, let me see them first.
unsetopt GLOB_DOTS        # Require a dot to match dotfiles.

# Misc
setopt MAIL_WARNING       # You have got mail.
setopt ALL_EXPORT         # Global export next sections variables.


################################################################################
# Maintain needed files.
#
#

if ! [ -f "$HOME/.zshrc.local" ]; then
    touch "$HOME/.zshrc.local"
fi
if ! [ -f "$HOME/.vimrc.local" ]; then
    touch "$HOME/.vimrc.local"
fi
if ! [ -f "$HOME/.settings/venv_locs" ]; then
    touch "$HOME/.settings/venv_locs"
fi


################################################################################
# Get and set general information about the environment
#
#

# What toaster/lawnmower/etc am I running this on?
if [[ -r "$HOME/.settings/scripts/detect-os" ]]; then
    source "$HOME/.settings/scripts/detect-os"
fi

if [ "$OSX" = "1" ]; then
    # Set a sane pretty print hostname and make tabs something reasonable.
    printf -- $'\033]6;1;bg;red;brightness;18\a\033]6;1;bg;green;brightness;18\a\033]6;1;bg;blue;brightness;18\a'
    HOSTNAME="local"
else
    HOSTNAME=$(hostname -s)
fi

PATH="/usr/local/git2.8/bin:$HOME/.local/bin:/usr/local/bin:/usr/local/sbin/:/bin:/sbin:/usr/bin:/usr/sbin:$PATH"
TZ="America/New_York"
HISTFILE=$HOME/.zhistory
HISTSIZE=5000
SAVEHIST=5000
PAGER='less'
EDITOR='vim'
MUTT_EDITOR='vim'
LC_ALL='en_US.UTF-8'
LANG='en_US.UTF-8'
LC_CTYPE=C
DOTFILES_DIR="$HOME/.dotfiles/dotfiles"

# Set our terminal titlebar to [$USER@$HOST]$ Current Running Command
FGCMD=''
PROMPT_COMMAND='printf "\033]0;[${USER}@${HOSTNAME}]\$ ${FGCMD}\x09\x09$(date +"(%m-%d %H:%M)")\007"'
precmd() { FGCMD='zsh'; eval "$PROMPT_COMMAND" }
preexec() { FGCMD="$1" ; eval "$PROMPT_COMMAND" }


################################################################################
# Colors ( Force Jellybeans )
#
#

echo -ne '\e]4;0;#121212\a'   # black
echo -ne '\e]4;1;#d75f5f\a'   # red
echo -ne '\e]4;2;#87af5f\a'   # green
echo -ne '\e]4;3;#ffd787\a'   # yellow
echo -ne '\e]4;4;#87afd7\a'   # blue
echo -ne '\e]4;5;#d7afff\a'   # magenta
echo -ne '\e]4;6;#5fd7ff\a'   # cyan
echo -ne '\e]4;7;#d7d7d7\a'   # white (light grey really)
echo -ne '\e]4;8;#686a66\a'   # bold black (i.e. dark grey)
echo -ne '\e]4;9;#f54235\a'   # bold red
echo -ne '\e]4;10;#99e343\a'  # bold green
echo -ne '\e]4;11;#fdeb61\a'  # bold yellow
echo -ne '\e]4;12;#84b0d8\a'  # bold blue
echo -ne '\e]4;13;#bc94b7\a'  # bold magenta
echo -ne '\e]4;14;#37e6e8\a'  # bold cyan
echo -ne '\e]4;15;#f1f1f0\a'  # bold white

autoload colors zsh/terminfo
if [[ "$terminfo[colors]" -ge 8 ]]; then
  colors
fi
for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
  eval PR_$color='$terminfo[bold]$fg[${(L)color}]'
  eval PR_LIGHT_$color='$fg[${(L)color}]'
  (( count = $count + 1 ))
done
PR_NO_COLOR="%{$terminfo[sgr0]%}"
PR_BOLD='[1m'
PR_DIM='[2m'
PR_UNDERLINE='[4m'
PR_BLINK='' # No.
PR_REVERSE='[7m'
PR_HIDDEN='[8m'
PR_NO='[0m'

# OSX/BSD
LSCOLORS="EafxcxdxcxegedaBagacad"
# Linux
LS_COLORS="di=1;34:ln=35:so=32:pi=33:ex=32:bd=34;46:cd=34;43:su=30;1;41:sg=30\
;46:tw=30;42:ow=30;43"

_ZSHRC_VENV_PATHS=$(cat "$HOME/.settings/venv_locs")


################################################################################
# Set the prompt
#
#

PS1="[%{$PR_BLUE%}%n%{$PR_WHITE%}@%{$PR_GREEN%}%U${HOSTNAME}%u%{$PR_NO_COLOR%}"
PS1="$PS1:%{$PR_RED%}%2c%{$PR_NO_COLOR%}]%(!.#.$) "
RPS1="%{$PR_LIGHT_YELLOW%}(%D{%m-%d %H:%M})%{$PR_NO_COLOR%}"


################################################################################
# Done with enviromental variables.
#
#

unsetopt ALL_EXPORT


################################################################################
# Aliases
#
#

alias s='ssh'
alias dog='~/.settings/scripts/dog.py'
alias quit='exit'
alias whereami='whoami && pwd'

# Auto virtualenv
alias cd="auto_venv_cd"

# Diff colorization and sane output.
alias diff="my_diff"
alias diff_color="perl -pe 's/^[^+-@](.*)$/$PR_DIM\1$PR_NO/gm|s/^\-(.*)$/$PR_RED\1$PR_NO/gm|s/^\+(.*)$/$PR_BLUE\1$PR_NO/gm|s/^@@ \-(\d+),\d+ \+(\d+),\d+ @@/Lines $PR_RED\1$PR_NO and $PR_BLUE\2$PR_NO\./gm'"

# LS colorization
if [ "$BSD" = "1" ]; then
  alias ll='ls -halG'
  alias ls='ls -G'
elif [ "$LINUX" = "1" ]; then
  alias ll='ls -hal --color=auto'
  alias ls='ls --color=auto'
else
    alias ll='ls -hal'
    alias ls='ls'
fi


################################################################################
# Keybinds
#
#

bindkey "^r" history-incremental-search-backward
bindkey ' ' magic-space    # also do history expansion on space
bindkey '^I' complete-word # complete on tab, leave expansion to _expand
bindkey "\e[1~" beginning-of-line # home
bindkey "\e[2~" insert # insert
bindkey "\e[3~" delete-char # delete
bindkey "\e[4~" end-of-line # end
bindkey "\e[5~" up-line-or-history # page up
bindkey "\e[6~" down-line-or-history # page down
bindkey "^H" backward-delete-char # Backspace
bindkey "^?" backward-delete-char # Also backspace, depending on terminal
# for freebsd console
bindkey "\e[H" beginning-of-line # Home
bindkey "\e[F" end-of-line # End

################################################################################
# Utility functions
#
#


# Automatically swap virtualenvs if nessesary
auto_venv_cd() {
    builtin cd "$@" 
    if [ -n "$_ZSHRC_VENV_PATHS" ]; then
        _ZSHRC_VENV_FOUND=""
        while read -r  _VENV_PATH; do
            if [[ "$PWD" == "$_VENV_PATH"* ]]; then
                if [ -n "$VIRTUAL_ENV" ] && [ "$VIRTUAL_ENV" != "$_VENV_PATH/env" ]; then
                    deactivate
                fi
                _ZSHRC_VENV_FOUND="Y"
                source "${_VENV_PATH}/env/bin/activate"
                break
            fi
        done <<< "$_ZSHRC_VENV_PATHS"
        if [ "$_ZSHRC_VENV_FOUND" = "" ] && [ -n "$VIRTUAL_ENV" ]; then
            deactivate
        fi
    fi
}

venv() {
    python3 -m venv env
    echo "${_ZSHRC_VENV_PATHS}\\n${PWD}/env" | sort -r >"${HOME}/.settings/venv_locs" 
    source "./env/bin/activate"
}

# Apply the appropriate colorization for diff.
my_diff() {
    if [ "$LINUX" = "1" ]; then
        /usr/bin/diff -u $@ | diff_color
    else
        /bin/diff -u $@ | diff_color
    fi
}

first-install() {
  if ! [ "$1" = "nogit" ]; then
    mkdir -p "$HOME/.dotfiles"
    git -C "$HOME/.dotfiles" clone "https://gitlab.com/mrcodysmith/dotfiles.git"
    update-symlinks
  else
    update-dotfiles nogit
    update-symlinks
  fi
}

# Handle re-symlinking everything between WC and home dir.
update-symlinks() {
    for internal_ilar_var_file in $DOTFILES_DIR/*(D); do
      if [ "$(basename $internal_ilar_var_file)" != ".git" ] && [ "$(basename $internal_ilar_var_file)" != ".gitignore" ]; then
        rm "${HOME}/$(basename $internal_ilar_var_file)" 2> /dev/null
        ln -s "$DOTFILES_DIR/$(basename $internal_ilar_var_file)" "${HOME}/$(basename $internal_ilar_var_file)"
      fi
    done
    source $HOME/.zshrc
}

# Either git-pull or grab a master copy and shove it over.
update-dotfiles() {
  if [ "$1" = "nogit" ]; then
    mkdir -p "$DOTFILES_DIR"
    curl "https://gitlab.com/MrCodySmith/dotfiles/-/archive/master/dotfiles-master.zip" -o "${HOME}/.dotfiles/update.zip"
    unzip -o "${HOME}/.dotfiles/update.zip" -d "${HOME}/.dotfiles/"
    cp -fr ${HOME}/.dotfiles/dotfiles-master/*(D) "$DOTFILES_DIR/"
  else
    git -C $DOTFILES_DIR pull
  fi
  update-symlinks
}

# Send a copy of this file to another host, then run first-install.
ssh-dotfiles() {
  scp -r "$DOTFILES_DIR/.zshrc" "$1:"
  echo "Please run first-install."
  ssh $1
}

# Push changes upstream.
push-dotfiles() {
  if [ -z "$1" ]; then
    echo "You must have a commit message."
  else
    for internal_ilar_var_file in $DOTFILES_DIR/*(D); do
        git -C $DOTFILES_DIR add "$internal_ilar_var_file"
    done
    git -C $DOTFILES_DIR commit -m "$1"
    git -C $DOTFILES_DIR push 
  fi
}


################################################################################
# Local files
#
#

source "${HOME}/.zshrc.local"
